#ifndef FOSAP_RDP_PARSER_H
#define FOSAP_RDP_PARSER_H
#include "Expr.h"
class Parser{
    private:
        char* str;
        int* pos;
        Expr* interpretedExpr;
        Expr* disjunktion();
        Expr* konjunktion();
        Expr* primitiv();

    public:
        explicit Parser(char *str, int* pos);
        Expr* interpret();
        bool valid();
        bool evaluate();
};
#endif //FOSAP_RDP_PARSER_H
