#define FALSES "/home/fatih/falses"
#define DEBUG false
#include <iostream>
#include <cstring>
#include "Parser.h"

#if DEBUG
//Debug, please ignore
int falseC = 0;
int trueC = 0;
#endif

int main(int argc, char** argv) {

    //Anzahl der Kommandozeilenparameter überprüfen
    if(argc != 2){
        printf("Erstes Argument muss Pfad zur Datei 'boolsche-audruecke' sein!\n");
        return 1;
    }


    //Dateistream öffnen
    FILE *fp;
    if((fp = fopen(argv[1], "r")) == nullptr){
        printf("Konnte Datei '%s' nicht öffnen!\n", argv[1]);
        return 1;
    }


#if DEBUG
    //Debug, please ignore
    if(DEBUG){
        remove(FALSES);
    }
#endif

    //Unser Zeichenpuffer, initalisiert mit einem exquisiten 'a'
    char ch = 'a';

    //Zeilenweise Datei iterieren
    while(ch != EOF){

        //Array der unsere Dateieingabe hält
        size_t size = 20;   //Größe unseres Puffers
        size_t len = 0;     //Aktuelle Anzahl an Elementen
        char *str = (char*) malloc(sizeof(char) * 20);
        if(!str){
            printf("Konnte keinen Speicher allokieren!\n");
            return 1;
        }

        //Buchstabe unserem Buffer hinzufügen
        while((ch = fgetc(fp)) != '\n' && ch != EOF){
            str[len++] = ch;

            //Prüfen ob unser Buffer voll ist
            if(len == size){

                //Buffer voll -> Erweitern
                str = (char*) realloc(str, sizeof(char) * (size += 32));
                if(!str){
                    printf("Konnte keinen Speicher allokieren!\n");
                    return 1;
                }
            }
        }

        //Hier einen Stringterminator anfügen
        str[len] = '\0';


        //Leere Eingaben ignorieren
        if(strcmp(str, "") != 0){

#if DEBUG
            //Debug, please ignore
            if(DEBUG){
                if(strcmp(str, "1&0|1") == 0){
                    printf("ALARM\n");
                }
            }
#endif

            //Eingabe mit dem Parser verarbeiten
            Parser parser = Parser(str, new int(0));
            //Erstmal prüfen, ob die Eingabe valid ist.
            if(parser.valid()){
                Expr* expr = parser.interpret();
                bool val = parser.evaluate();

                if(val){
                    printf("True\n");
#if DEBUG
                    trueC++;
#endif
                }
                else{
                    printf("False\n");
#if DEBUG
                    //Falsche Ausgaben in eine Ausgabedatei schreiben
                    //Debug, please ignore
                    if(DEBUG){
                        FILE *fop;

                        if((fop = fopen(FALSES, "a")) != nullptr){
                            fputs("\n", fop);
                            fputs(str, fop);
                            fflush(fop);
                        }
                        else{
                            printf("Konnte Falses nicht schreiben!\n");
                        }
                        fclose(fop);
                    }

                    falseC++;
#endif
                }

                delete expr;
            }
        }

        //Speicherbereiche freigeben
        free(str);
    }
    fclose(fp);


#if DEBUG
    //Debug, please ignore
    if(DEBUG){
        printf("True  Count: %d\n", trueC);
        printf("False Count: %d\n", falseC);
    }
#endif


    return 0;
}
