#include "Expr.h"

//region Negation
Negation::Negation(Expr *expr) {
    this->expr = expr;
}

bool Negation::evaluate() {
    bool retVal = !this->expr->evaluate();
    return retVal;
}

Negation::~Negation() {
    delete this->expr;
}
//endregion



//region Gruppierung
Gruppierung::Gruppierung(Expr *expr) {
    this->expr = expr;
}

bool Gruppierung::evaluate() {
    bool retVal = this->expr->evaluate();
    return retVal;
}

Gruppierung::~Gruppierung() {
    delete this->expr;
}
//endregion



//region Konjunktion
Konjunktion::Konjunktion(Expr *left, Expr *right) {
    this->left = left;
    this->right = right;
}

bool Konjunktion::evaluate() {
    bool retVal = ( this->left->evaluate() && this->right->evaluate() );
    return retVal;
}

Konjunktion::~Konjunktion() {
    delete this->left;
    delete this->right;
}
//endregion



//region Disjunktion
Disjunktion::Disjunktion(Expr *left, Expr *right) {
    this->left = left;
    this->right = right;
}
bool Disjunktion::evaluate() {
    bool retVal = ( this->left->evaluate() || this->right->evaluate() );
    return retVal;
}

Disjunktion::~Disjunktion() {
    delete this->left;
    delete this->right;
}
//endregion



//region Literal
Literal::Literal(bool value) {
    this->value = value;
}

bool Literal::evaluate() {
    return this->value;
}

Literal::~Literal() {}
//endregion