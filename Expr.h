#ifndef FOSAP_RDP_EXPR_H
#define FOSAP_RDP_EXPR_H
class Expr{
    public:
        virtual bool evaluate() = 0;
        virtual ~Expr() = default;
};

class Negation: public Expr{
    private:
        Expr* expr;
    public:
        explicit Negation(Expr* expr);
        ~Negation() override;
        bool evaluate() override;
};

class Gruppierung: public Expr{
    private:
        Expr* expr;
    public:
        explicit Gruppierung(Expr* expr);
        ~Gruppierung() override;
        bool evaluate() override;
};

class Konjunktion: public Expr{
    private:
        Expr* left;
        Expr* right;
    public:
        explicit Konjunktion(Expr* left, Expr* right);
        ~Konjunktion() override;
        bool evaluate() override;
};

class Disjunktion: public Expr{
    private:
        Expr* left;
        Expr* right;
    public:
        explicit Disjunktion(Expr* left, Expr* right);
        ~Disjunktion() override;
        bool evaluate() override;
};

class Literal : public Expr{
    private:
        bool value;
    public:
        explicit Literal(bool value);
        ~Literal() override;
        bool evaluate() override;
};
#endif //FOSAP_RDP_EXPR_H