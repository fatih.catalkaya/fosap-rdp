#include <cstdio>
#include "Parser.h"
#include "Expr.h"


/**
 * Konstruktor
 * @param str
 * @param pos
 */
Parser::Parser(char *str, int* pos) {
    this->str = str;
    this->pos = pos;
}



Expr *Parser::disjunktion() {
    Expr* expr = konjunktion();

    if(str[(*pos)] == '|'){
        (*pos)++;   //Das '|' wurde konsumiert

        Expr* right = disjunktion();
        expr = new Disjunktion(expr, right);
    }

    return expr;
}


Expr *Parser::konjunktion() {
    Expr* expr = primitiv();

    if(str[(*pos)] == '&'){
        (*pos)++;   //Das '&' wurde konsumiert

        Expr* right = konjunktion();
        expr = new Konjunktion(expr, right);
    }

    return expr;
}


Expr *Parser::primitiv() {
    switch (str[(*pos)]) {
        case '0': {
            (*pos)++;   // 0 konsumieren
            return new Literal(false);
        }

        case '1': {
            (*pos)++;   // 1 konsumieren
            return new Literal(true);
        }

        case '(': {
            (*pos)++;   // ( konsumieren
            Expr *disj = disjunktion();
            Expr *grup = new Gruppierung(disj);
            (*pos)++;   // ) konsumieren
            return grup;
        }

        case '~': {
            (*pos)++;   // ~ konsumieren
            Expr *prim = primitiv();
            return new Negation(prim);
        }
    }
}


/**
 * Erstellt den Expr-Baum aus der Eingabe
 * @return
 */
Expr* Parser::interpret() {
    this->interpretedExpr = disjunktion();
    return this->interpretedExpr;
}


/**
 * Evaluiert den Expr-Baum aus der Eingabe
 * @return Result of Evaluation
 */
bool Parser::evaluate() {
    return this->interpretedExpr->evaluate();
}


/**
 * Prüft, ob die Eingabe valid ist
 * @return Validity
 */
bool Parser::valid() {
    int i = 0;
    int* klammerCounter = new int(0);

    //Durch den String iterieren
    while(this->str[i] != '\0'){
        // Prüfen, ob unbekanntes Zeichen im String enthalten ist und dass keine Symbole
        // doppelt vorkommen. Außerdem werden die Klammern gezählt.
        switch (str[i]) {
            case '0':
                //Prüfen, dass das nächste Zeichen kein 0 oder 1 ist
                if(str[i+1] == '1' || str[i+1] == '0'){
                    printf("Unerwartetes Symbol '%c' an Stelle %d!\n", str[i+1], i+2);
                    printf("Eingabe war:\n%s\n", str);
                    return false;
                }
                break;
            case '1':
                //Prüfen, dass das nächste Zeichen kein 0 oder 1 ist
                if(str[i+1] == '1' || str[i+1] == '0'){
                    printf("Unerwartetes Symbol '%c' an Stelle %d!\n", str[i+1], i+2);
                    printf("Eingabe war:\n%s\n", str);
                    return false;
                }
                break;
            case '&':
                //Prüfen, dass das nächste Zeichen kein | oder & ist
                if(str[i+1] == '|' || str[i+1] == '&'){
                    printf("Unerwartetes Symbol '%c' an Stelle %d!\n", str[i+1], i+2);
                    printf("Eingabe war:\n%s\n", str);
                    return false;
                }
                break;
            case '|':
                //Prüfen, dass das nächste Zeichen kein | oder & ist
                if(str[i+1] == '|' || str[i+1] == '&'){
                    printf("Unerwartetes Symbol '%c' an Stelle %d!\n", str[i+1], i+2);
                    printf("Eingabe war:\n%s\n", str);
                    return false;
                }
                break;
            case '(':
                (*klammerCounter)++;
                break;
            case ')':
                (*klammerCounter)--;
                break;
            case '~':
                // Die Negation muss hier leer drin stehen, damit nicht versehentlich
                // Der Default-Teil ausgeführt wird.
                break;
            default:
                printf("Unerwartetes Symbol '%c' an Stelle %d!\n", str[i], i);
                printf("Eingabe war:\n%s\n", str);
                return false;
        }

        //I erhöhen
        i++;
    }

    if((*klammerCounter) != 0){
        printf("Falsche Klammerung! ");

        if((*klammerCounter) > 0){
            printf("Zu viele Klammern!\n");
        }
        if((*klammerCounter) < 0){
            printf("Zu wenig Klammern!\n");
        }

        return false;
    }

    return true;
}

